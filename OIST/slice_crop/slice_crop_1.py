from ij import IJ, ImagePlus, ImageStack  
from ij.io import FileSaver
import os

# Load a stack of images: a fly brain, in RGB  

image_range_0 = 1
image_range_e = 30

c1_z0_list = [64,24]
c1_ze_list = [151,95]           # The last slice wanted to KEEP
roi_x0_list = [130,158]
roi_y0_list = [50,22]
roi_xe_list = [216,332]
roi_ye_list = [226,124]

folder = '/home/yujie/work/OIST/Cell tracking/slice_crop/'
file_basename = "HeLa_CF_1_s1_t"
print folder + file_basename

for image_num in range(image_range_0, image_range_e + 1):
    filename = file_basename + str(image_num) + '.TIF'
    imp = IJ.openImage(folder + filename)
    stack = imp.getImageStack()  
    # Iterate each slice in the stack

    for cn in range(len(c1_z0_list)):  # crop number
        
        c1 = []                         # cropped stack
        c1_z0 = c1_z0_list[cn]
        c1_ze = c1_ze_list[cn]
        roi_x0 = roi_x0_list[cn]
        roi_y0 = roi_y0_list[cn]
        roi_xe = roi_xe_list[cn]
        roi_ye = roi_ye_list[cn]
        roi_w = roi_xe - roi_x0
        roi_h = roi_ye - roi_y0
        
        for i in xrange(c1_z0, c1_ze+1):  
            s1 = stack.getProcessor(i)
            s1.setRoi(roi_x0, roi_y0, roi_w, roi_h)
            s2 = s1.crop()
            # ... and store it in the prepared empty list 
            c1.append(s2)  

        stack2 = ImageStack(roi_w, roi_h)  

        for fp in c1:
            stack2.addSlice(None, fp)  

        # Create a new image with the stack of green channel slices  
        imp2 = ImagePlus("", stack2)  
        # Set a green look-up table:  
        # IJ.run(imp2, "Green", "")  
        # imp2.show()  

        fs = FileSaver(imp2)

        saveDir = folder + file_basename + '_c' + str(cn)
        file_savename = file_basename + str(image_num) + 'c' + str(cn) + '.TIF'
        # Test if the folder exists before attempting to save the image:
        if not os.path.exists(saveDir):
            os.makedirs(saveDir)
        if os.path.exists(saveDir) and os.path.isdir(saveDir):  
          print "folder exists:", saveDir  
          filepath = os.path.join(saveDir, file_savename) # Operating System-specific  
          if os.path.exists(filepath):  
            print "File exists! Not saving the image, would overwrite a file!"  
          elif fs.saveAsTiff(filepath):  
            print "File saved successfully at ", filepath  
        else:  
          print "Folder does not exist or it's not a folder!"  



# # Print image details  
# print "title:", imp.title  
# print "width:", imp.width  
# print "height:", imp.height  
# print "number of pixels:", imp.width * imp.height  
# print "number of slices:", imp.getNSlices()  
# print "number of channels:", imp.getNChannels()  
# print "number of time frames:", imp.getNFrames()  
