import sys
import glob
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math
import os
# import plotly.plotly as py  # tools to communicate with Plotly's server

from openpyxl import load_workbook
from openpyxl.utils import get_column_letter, column_index_from_string

def log_criteria(title):
    if str(title) == 'Angular frequency' or 'Oscillation strain':
        return True
    else:
        return False


font = { 'weight' : 'normal',
        'size'   : 22}
matplotlib.rc('font', **font)
matplotlib.rc('axes', titlesize=22)
matplotlib.rc('legend', fontsize=14)
matplotlib.rc('xtick', labelsize=16)
matplotlib.rc('ytick', labelsize=16)


result_folder = './result/'
if not os.path.exists(result_folder):
    os.makedirs(result_folder)


figsizex = 18
figsizey = 12



'''
The input list:
wb_name_list, sheet_num_list, col_s_list, row_s_list, x_num_list

!!! Could have design a smarter way in searching the 'Angular frequency' and get col_s and row_s !!!
!!! But maybe this one is also good as it is easy to single out a dataframe you want!!!

wb_name is the file name
sheet number start from 0
col_s_list contains letter of col, like 'A'
row_s_list are numbers starting from the title
x_num, the col used as x, is counted from 0 as the relative position in the dataframe
'''
input_list = [
    
    ['12-2.xlsx', 0, 'A', 3, 0],
    ['12-2.xlsx', 0, 'M', 3, 1],
    ['12-2.xlsx', 0, 'W', 3, 1],
    ['12-2.xlsx', 1, 'A', 3, 1],
    ['12-2.xlsx', 2, 'A', 3, 1],

    ['40C.xlsx', 0, 'A',  4, 0],
    ['40C.xlsx', 0, 'K',  4, 1],
    ['40C.xlsx', 0, 'U',  4, 1],
    ['40C.xlsx', 0, 'AE',  4, 1],
    ['40C.xlsx', 0, 'AO',  4, 0],
    ['40C.xlsx', 0, 'AY',  4, 3],

    ['H111.xlsx', 0, 'A',  4, 0],
    ['H111.xlsx', 0, 'K',  4, 1],
    ['H111.xlsx', 0, 'U',  4, 1],
    ['H111.xlsx', 0, 'AE',  4, 1],
    ['H111.xlsx', 0, 'AO',  4, 0],
    ['H111.xlsx', 0, 'AY',  4, 3],

    ['op06-2.xlsx', 0, 'A',  4, 0],
    ['op06-2.xlsx', 0, 'K',  4, 1],
    ['op06-2.xlsx', 0, 'U',  4, 1],
    ['op06-2.xlsx', 0, 'AE',  4, 1],
    ['op06-2.xlsx', 0, 'AO',  4, 0],
    ['op06-2.xlsx', 0, 'AY',  4, 3],
    
    ['p26-26.xlsx', 0, 'A',  4, 0],
    ['p26-26.xlsx', 0, 'K',  4, 1],
    ['p26-26.xlsx', 0, 'U',  4, 1],
    ['p26-26.xlsx', 0, 'AE',  4, 1],
    ['p26-26.xlsx', 0, 'AO',  4, 0],
    ['p26-26.xlsx', 0, 'AY',  4, 3],

    ['p-26-3.xlsx', 0, 'A',  4, 0],
    ['p-26-3.xlsx', 0, 'K',  4, 1],
    ['p-26-3.xlsx', 0, 'U',  4, 1],
    ['p-26-3.xlsx', 0, 'AE',  4, 1],
    ['p-26-3.xlsx', 0, 'AO',  4, 0],
    ['p-26-3.xlsx', 0, 'AY',  4, 3],


    ['p-26-4.xlsx', 0, 'A',  3, 0],
    ['p-26-4.xlsx', 0, 'M',  3, 1],
    ['p-26-4.xlsx', 0, 'W',  3, 1],
    ['p-26-4.xlsx', 1, 'A',  3, 1],
    ['p-26-4.xlsx', 2, 'L',  53, 1],

    ['zy.xlsx', 0, 'A',  3, 0], 
    ['zy.xlsx', 0, 'K',  3, 1], 
    ['zy.xlsx', 0, 'U',  3, 1], 
    ['zy.xlsx', 0, 'AE',  3, 1], 
    ['zy.xlsx', 0, 'AO',  3, 0], 
    ['zy.xlsx', 0, 'AY',  3, 1], 
    ['zy.xlsx', 0, 'BI',  3, 1], 
    ['zy.xlsx', 0, 'BS',  3, 1], 
    ['zy.xlsx', 0, 'CC',  3, 0], 
    ['zy.xlsx', 0, 'CM',  3, 0], 
    ['zy.xlsx', 0, 'CW',  3, 1], 
    ['zy.xlsx', 0, 'DG',  3, 1], 
    ['zy.xlsx', 0, 'DQ',  3, 1], 
    ['zy.xlsx', 0, 'EA',  3, 1]

    
]


for wb_name, sheet_num, col_s, row_s, x_num in input_list:
    wb = load_workbook(filename = wb_name) # loop

    # use upper-left corner cell, e.g., 'at= A3', to specify dataframe name in a given book and sheet 
    fig1_name= wb_name + '_sn=' + str(sheet_num) + '_at=' + col_s + str(row_s) +'_modulus' # loop
    fig2_name= wb_name + '_sn=' + str(sheet_num) + '_at=' + col_s + str(row_s) +'_viscosity' # loop

    sn = wb.get_sheet_names()[sheet_num] # loop
    ws = wb.get_sheet_by_name(sn)

    # determine col_e and row_e instead of checking manually
    '''
    check column by colmun at row_s, start at col_s
    check row by row by at col_s, start at row_s
    '''
    for col in range(column_index_from_string(col_s), ws.max_column+2): # +2, because range need +1, and need to check the next empty col
        if(ws.cell(row_s, col).value is None):
            col_e = col - 1
            col_e = get_column_letter(col_e) # convert number to letter. Stupid excel!
            break

    for row in range(row_s, ws.max_row + 2): 
        if(ws.cell(row,column_index_from_string(col_s)).value is None):
            row_e = row - 1
            break
            
    title = ws[col_s + str(row_s) : col_e + str(row_s)] # loop
    unit = ws[col_s + str(row_s+1) : col_e + str(row_s+1)] # loop
    data = ws[col_s + str(row_s+2) : col_e + str(row_e)]   # loop

    # x_num = 1 # commented for loop 
    
    # x = [j[x_num].value for j in data] 

    # modulus and tanδ
    fig1, ax1 = plt.subplots()

    
    cdl_num = [x_num, 6, 7, 5, 8]  # coordinate data lists, x, storage, loss, tan δ, viscosity, will be assigned to range(0,5)
    '''
    The above, need to use redundant setting that cdl_num[0] == x_num, so that cdl_num[] and cdl[] will correspond to each other.
    Also, I have to filter out those x with wrong y, so cdl[] must include the list of x
    '''
    
    ax1.set_xlabel(str(title[0][x_num].value)+' ('+str(unit[0][x_num].value)+')')

    x_sm_lm_d_v = [[j[cdl_num[0]].value, j[cdl_num[1]].value, j[cdl_num[2]].value, j[cdl_num[3]].value, j[cdl_num[4]].value] for j in data]

    cdl=[]
    for i in range(5):
        cdl.append([ k[i] for k in x_sm_lm_d_v if all(k[i] > 0 for i in range(1,5)) ])

    lns0 = []
    for i in [1,2]:
        lns0.append(ax1.plot(cdl[0],cdl[i],'o-',label=str(title[0][cdl_num[i]].value)))
    ax1.set_ylabel('Modulus'+' ('+str(unit[0][cdl_num[i]].value)+')')
    # ax1.set_xlim([x_lower_limit, x_upper_limit])
    # ax1.set_ylim([y_lower_limit1, y_upper_limit1])

    if log_criteria(title[0][x_num].value):
        ax1.set_xscale("log", nonposx='clip')
    ax1.set_yscale("log", nonposy='clip')

    ax2 = ax1.twinx()
    lns1 = ax2.plot(cdl[0],cdl[3],'ro-',label=str(title[0][cdl_num[3]].value))
    ax2.set_ylabel(str(title[0][cdl_num[3]].value))
    # ax2.set_ylim([y_lower_limit2, y_upper_limit2])

    lns = lns0[0]+lns0[1]+lns1
    labs = [l.get_label() for l in lns]
    ax1.legend(lns, labs, loc=9, bbox_to_anchor=(0.5, -0.1), ncol=3)

    fig1.tight_layout()
    fig1.set_size_inches(figsizex, figsizey)
    fig1.savefig(result_folder + str(fig1_name) + '.pdf', dpi=300,bbox_inches='tight')

    fig1.clf()


    # viscosity
    fig2, ax3 = plt.subplots()
    ax3.set_xlabel(str(title[0][x_num].value)+' ('+str(unit[0][x_num].value)+')')

    ax3.plot(cdl[0],cdl[4],'o-',label=str(title[0][cdl_num[4]].value))
    ax3.set_ylabel(str(title[0][cdl_num[4]].value) + ' ('+str(unit[0][cdl_num[4]].value)+')')
    if log_criteria(title[0][x_num].value):
        ax3.set_xscale("log", nonposx='clip')
    ax3.set_yscale("log", nonposy='clip')

    # ax3.set_xlim([x_lower_limit, x_upper_limit])
    # ax3.set_ylim([y_lower_limit1, y_upper_limit1])

    # ax3.legend(loc='best')

    fig2.tight_layout()
    fig2.set_size_inches(figsizex, figsizey)
    fig2.savefig(result_folder + str(fig2_name) + '.pdf', dpi=300,bbox_inches='tight')

    fig2.clf()

