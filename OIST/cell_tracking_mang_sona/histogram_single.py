import sys
import glob
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
# import plotly.plotly as py  # tools to communicate with Plotly's server

font = { 'weight' : 'normal',
        'size'   : 22}
matplotlib.rc('font', **font)
matplotlib.rc('axes', titlesize=22)
matplotlib.rc('legend', fontsize=16)
matplotlib.rc('xtick', labelsize=16)
matplotlib.rc('ytick', labelsize=16)

dirArg1 = sys.argv[1]
list1=[]
for filenames in glob.glob(str(dirArg1) + '/**/*.tsv',recursive = True):
    listImport = np.loadtxt(filenames,skiprows=1)
    list1.extend(list(listImport[listImport[:,-2]>0,-2]))


fig, ax = plt.subplots()
nBins = 60
bins = np.linspace(0, nBins, nBins+1)

ax.hist(list1, bins=bins, rwidth=0.9,density=True,label='Gel')

ticks = range(0, nBins+2, 5)
ax.set_xticks(ticks)
ax.legend()
ax.set_title('Cell tracking')
ax.set_xlabel(r'Speed ($\mu m/hr$)')
ax.set_ylabel('Relative frequency')

# plt.show()
fig.savefig('single.pdf', bbox_inches='tight')

